from django import template

register = template.Library()

@register.simple_tag
def progress_bar(choice):
    progress_bar = '<div class="progress progress-striped active"><div class="bar" style="width: {0}%;"></div></div>'.format(choice.percent_voted())
    return progress_bar