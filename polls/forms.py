from django import forms
from django.forms.models import BaseInlineFormSet
from polls.models import Choice, Poll


class VoteForm(forms.Form):
    choice = forms.ModelChoiceField(
        queryset=Choice.objects.all(),
        widget=forms.RadioSelect,
        empty_label=None,
    )

    def __init__(self, *args, **kwargs):
        poll = kwargs.pop('poll')
        super(VoteForm, self).__init__(*args, **kwargs)
        self.fields['choice'].queryset = poll.choice_set.all()


class PollForm(forms.ModelForm):
    class Meta():
        model = Poll
        fields = (
            'question',
            )


class BaseFormSet(BaseInlineFormSet):
    def add_fields(self, form, index):
        super(BaseFormSet, self).add_fields(form, index)
        form.fields['ORDER'].widget = forms.HiddenInput()
