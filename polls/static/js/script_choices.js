$(function() {     
    /*
       Make choices sortable.
    */      
    $('#sortable').sortable( {
        stop: function(ui, event){  
            $(".li_sortable").each(function() {
                var order = parseInt($(this).index()) + 1;
                $(this).children("input[name$='ORDER']").val(order);
            });                                      
        }
    });
});