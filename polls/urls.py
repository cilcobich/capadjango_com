from django.conf.urls import patterns, url
from polls import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^(?P<poll_id>\d+)/vote/$', views.vote, name='vote'),
    url(r'^(?P<poll_id>\d+)/result/$', views.result, name='id_result'),
    url(r'^results/$', views.result, name='results'),
    url(r'^more_voted/$', views.more_voted, name='more_voted'),
    url(r'^vote_average/$', views.vote_average, name='vote_average'),
    url(r'^new_poll/$', views.poll_manage, name='new_poll'),
    url(r'^edit_poll/(?P<poll_id>\d+)/$', views.poll_manage, name='edit_poll'),
    url(r'^error/$', views.error, name='error'),
    #Logueo y Deslogueo
    url(r'^logueo/$', 'django.contrib.auth.views.login', {'template_name': 'polls/logueo.html'}, name='logueo'),
    url(r'^deslogueo/$', 'django.contrib.auth.views.logout', {'next_page': '/polls'}, name='deslogueo'),
)
