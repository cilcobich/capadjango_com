import datetime
from django.utils import timezone
from django.db import models
from django.db.models import Sum, Avg
from django.contrib.auth.models import User


class Perfil(models.Model):
    direccion = models.CharField(max_length=150, null=True)
    telefono = models.CharField(max_length=150, null=True)
    user = models.OneToOneField(User, unique=True, related_name='perfil')

    def __unicode__(self):
        return self.user.username


class PollManager(models.Manager):
    def more_voted(self):
        polls = self.annotate(votes_count=Sum('choice__votes'))
        if polls:
            poll_more_voted = max(polls, key=lambda p: p.votes_count)
            more_voted = {'poll': poll_more_voted, 'total': poll_more_voted.votes_count}
            return more_voted
        else:
            return {}

    def vote_average(self):
        polls = self.annotate(votes_count=Sum('choice__votes'))
        if polls:
            vote_average = polls.aggregate(Avg('votes_count'))
            return vote_average['votes_count__avg']
        else:
            return 0


class Poll(models.Model):
    objects = PollManager()

    question = models.CharField(max_length=200)
    pub_date = models.DateTimeField(default=timezone.now)

    def __unicode__(self):
        return self.question

    class Meta:
        permissions = (
            ("puede_votar", "Puede Votar"),
        )

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

    def total_votes(self):
        if self.choice_set.all().count() > 0:
            total_votes = self.choice_set.aggregate(total=Sum('votes'))
            return total_votes['total']
        return 0


class Choice(models.Model):
    poll = models.ForeignKey(Poll)
    choice = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    class Meta:
        order_with_respect_to = 'poll'

    def __unicode__(self):
        return self.choice

    def percent_voted(self):
        total_votes = self.poll.total_votes()
        if total_votes > 0:
            return self.votes * 100 / total_votes
        return 0
