from django.test import TestCase
from django.contrib.auth.models import Permission
from django.core.urlresolvers import reverse
from django.test.client import Client
from model_factories import PollFactory, ChoiceFactory
from django.utils import timezone
from django.contrib.auth.models import User
from polls.models import Poll, Choice
from polls.forms import VoteForm, PollForm, BaseFormSet
from django.forms.models import inlineformset_factory
import datetime


def clear_db():
    Poll.objects.all().delete()  # Clear database


class PollsModelTest(TestCase):

    def test_was_published_recently(self):
        p = PollFactory.build()
        self.assertTrue(p.was_published_recently())
        today = timezone.now() - datetime.timedelta(hours=23, seconds=59)
        p = PollFactory.build(pub_date=today)
        self.assertTrue(p.was_published_recently())

    def test_was_not_published_recently(self):
        yesterday = timezone.now() - datetime.timedelta(days=1, seconds=1)
        p = PollFactory.build(pub_date=yesterday)
        self.assertFalse(p.was_published_recently())

    def test_default_total_votes(self):
        p = PollFactory.build()
        self.assertEqual(p.total_votes(), 0)

    def test_total_votes(self):
        p = PollFactory()
        ChoiceFactory(poll=p, votes=1)
        ChoiceFactory(poll=p, votes=1)
        ChoiceFactory(poll=p, votes=0)
        self.assertEqual(p.total_votes(), 2)

    def test_default_more_voted(self):
        more_voted = Poll.objects.more_voted()
        self.assertEqual(more_voted, {})

    def test_more_voted(self):
        poll_1 = PollFactory()
        ChoiceFactory(poll=poll_1, votes=1)
        ChoiceFactory(poll=poll_1, votes=5)
        poll_2 = PollFactory()
        ChoiceFactory(poll=poll_2, votes=3)
        ChoiceFactory(poll=poll_2, votes=5)

        more_voted = Poll.objects.more_voted()
        self.assertEqual(more_voted['total'], 8)

    def test_default_vote_average(self):
        vote_average = Poll.objects.vote_average()
        self.assertEqual(vote_average, 0)

    def test_vote_average(self):
        poll_1 = PollFactory()
        ChoiceFactory(poll=poll_1, votes=1)
        ChoiceFactory(poll=poll_1, votes=1)
        poll_2 = PollFactory()
        ChoiceFactory(poll=poll_2, votes=1)
        ChoiceFactory(poll=poll_2, votes=1)

        vote_average = Poll.objects.vote_average()
        self.assertEqual(vote_average, 2)

    def tearDown(self):
        clear_db()


class ChoiceModelTest(TestCase):

    def test_default_percent_voted(self):
        poll_1 = PollFactory()
        choice_1 = ChoiceFactory(poll=poll_1)

        percent_voted_choice_1 = choice_1.percent_voted()
        self.assertEqual(percent_voted_choice_1, 0)

    def test_percent_voted(self):
        poll_1 = PollFactory()
        choice_1 = ChoiceFactory(poll=poll_1, votes=1)
        choice_2 = ChoiceFactory(poll=poll_1, votes=3)

        percent_voted_choice_1 = choice_1.percent_voted()
        percent_voted_choice_2 = choice_2.percent_voted()
        self.assertEqual(percent_voted_choice_1, 25)
        self.assertEqual(percent_voted_choice_2, 75)

    def tearDown(self):
        clear_db()


class PollsViewsTest(TestCase):
    username_default = "default"
    password_default = "password"
    username_votante = "votante"
    password_votante = "password"
    username_admin = "admin"
    password_admin = "password"
    prepared = False

    def setUp(self):
        "Initial Setup"
        if not self.prepared:
            user_default, created = User.objects.get_or_create(username=self.username_default)
            user_default.set_password(self.password_default)
            user_default.save()

            user_votante, created = User.objects.get_or_create(username=self.username_votante)
            user_votante.set_password(self.password_votante)
            user_votante.save()
            user_votante.user_permissions.add(Permission.objects.get(codename='puede_votar'))

            user_admin, created = User.objects.get_or_create(username=self.username_admin)
            user_admin.set_password(self.password_admin)
            user_admin.save()
            user_admin.user_permissions.add(
                Permission.objects.get(codename='add_poll'),
                Permission.objects.get(codename='add_choice'),
            )

            self.client = Client()

            self.polls = [PollFactory() for i in range(10)]
            for poll in self.polls:
                [ChoiceFactory(poll=poll, votes=i * i) for i in range(3)]

                self.prepared = True

    def test_empty_index_view(self):
        clear_db()
        url = reverse('polls:index')
        resp = self.client.get(url)
        self.assertContains(resp, 'No hay encuestas.')
        self.assertTemplateUsed(resp, 'polls/index.html')

    def test_get_index_view(self):
        url = reverse('polls:index')
        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 200)

        self.assertTrue('latest_poll_list' in resp.context)
        polls = [k in self.polls for k in resp.context['latest_poll_list']]
        self.assertTrue(all(polls))
        self.assertTemplateUsed(resp, 'polls/index.html')

    def test_get_result_view(self):
        url = reverse('polls:results')
        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 200)
        self.assertTrue('polls' in resp.context)
        self.assertTemplateUsed(resp, 'polls/results.html')

    def test_get_id_result_view(self):
        url = reverse('polls:id_result', kwargs={'poll_id': 1})
        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 200)
        self.assertTrue('polls' in resp.context)
        self.assertTemplateUsed(resp, 'polls/results.html')

    def test_get_was_not_id_result_view(self):
        url = reverse('polls:id_result', kwargs={'poll_id': 11})
        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 404)
        self.assertFalse('polls' in resp.context)
        self.assertTemplateUsed(resp, '404.html')

    def test_get_more_voted_view(self):
        url = reverse('polls:more_voted')
        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 200)
        self.assertTrue('more_voted' in resp.context)
        self.assertTemplateUsed(resp, 'polls/more_voted.html')

    def test_get_vote_average_view(self):
        url = reverse('polls:vote_average')
        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 200)
        self.assertTrue('vote_average' in resp.context)
        self.assertTemplateUsed(resp, 'polls/vote_average.html')

    ######################################
    # Testing views when user is logged in
    ######################################

    def test_get_poll_vote_view(self):
        self.client.login(username=self.username_votante, password=self.password_votante)

        poll_id = self.polls[0].pk
        url = reverse('polls:vote', kwargs={'poll_id': poll_id})
        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'polls/vote.html')
        self.assertEqual(resp.context['poll'].pk, poll_id)

    def test_get_no_perm_poll_vote_view(self):
        self.client.login(username=self.username_default, password=self.password_default)

        poll_id = self.polls[0].pk
        url = reverse('polls:vote', kwargs={'poll_id': poll_id})
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 302)

    def test_post_poll_vote_view(self):
        self.client.login(username=self.username_votante, password=self.password_votante)

        poll_id = self.polls[0].pk
        voted_choice = self.polls[0].choice_set.all()[0]
        self.assertEqual(voted_choice.votes, 0)

        url = reverse('polls:vote', kwargs={'poll_id': poll_id})
        response = self.client.post(url, {'choice': voted_choice.pk})
        self.assertEqual(self.polls[0].choice_set.all()[0].votes, 1)

        url = reverse('polls:id_result', kwargs={'poll_id': poll_id})
        self.assertRedirects(response, url)

    def test_post_no_perm_poll_vote_view(self):
        self.client.login(username=self.username_default, password=self.password_default)

        poll_id = self.polls[0].pk
        voted_choice = self.polls[0].choice_set.all()[0]
        self.assertEqual(voted_choice.votes, 0)

        url = reverse('polls:vote', kwargs={'poll_id': poll_id})
        self.client.post(url, {'choice': voted_choice.pk})
        self.assertEqual(self.polls[0].choice_set.all()[0].votes, 0)

    def test_get_poll_manage_edit_view(self):
        self.client.login(username=self.username_admin, password=self.password_admin)

        poll_id = self.polls[0].pk
        url = reverse('polls:edit_poll', kwargs={'poll_id': poll_id})
        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'polls/poll.html')
        self.assertTrue('form', 'formset_choice_poll' in resp.context)

    def test_get_no_perm_poll_manage_edit_view(self):
        self.client.login(username=self.username_default, password=self.password_default)

        poll_id = self.polls[0].pk
        url = reverse('polls:edit_poll', kwargs={'poll_id': poll_id})
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 302)

    def test_post_poll_manage_new_view(self):
        self.client.login(username=self.username_admin, password=self.password_admin)

        url = reverse('polls:new_poll')
        post_data = {'choice_set-MAX_NUM_FORMS': u'',
                     'choice_set-0-ORDER': u'',
                     'choice_set-0-choice': u'Oliva',
                     'choice_set-1-id': u'',
                     'choice_set-1-choice': u'Girasol',
                     'choice_set-INITIAL_FORMS': u'0',
                     'choice_set-0-poll': u'',
                     'choice_set-1-ORDER': u'',
                     'choice_set-0-id': u'',
                     'choice_set-1-poll': u'',
                     'choice_set-TOTAL_FORMS': u'2',
                     'question': u'Aceite de Oliva o Girasol',
                     'guarda_poll': u'',
                     }
        response = self.client.post(url, post_data)

        url = reverse('polls:index')
        self.assertRedirects(response, url)

        poll = Poll.objects.all().order_by('-pk')[0]
        self.assertEqual(poll.question, u'Aceite de Oliva o Girasol')

        choices = poll.choice_set.all()
        self.assertEqual(choices.count(), 2)
        self.assertTrue('Oliva', 'Girasol' in choices)
