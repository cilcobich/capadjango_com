from django.shortcuts import render, redirect, get_object_or_404, get_list_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.forms.models import inlineformset_factory
from django.views.generic import ListView, FormView, TemplateView
from django.utils.decorators import method_decorator

from polls.models import Poll, Choice
from polls.forms import VoteForm, PollForm, BaseFormSet


class LatestPolls(ListView):
    queryset = Poll.objects.order_by('-pub_date')[:5]
    context_object_name = 'latest_poll_list'
    template_name = 'polls/index.html'
index = LatestPolls.as_view()


class ResultPolls(ListView):
    def get_queryset(self, **kwargs):
        if 'poll_id' in self.kwargs:
            return get_list_or_404(Poll, pk=self.kwargs['poll_id'])
        else:
            return Poll.objects.all()
    context_object_name = 'polls'
    template_name = 'polls/results.html'
result = ResultPolls.as_view()


class MoreVotedPolls(TemplateView):
    model = Poll
    context_object_name = 'more_voted'
    template_name = 'polls/more_voted.html'

    def dispatch(self, request, *args, **kwargs):
        self.more_voted = Poll.objects.more_voted()
        return super(MoreVotedPolls, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(MoreVotedPolls, self).get_context_data(**kwargs)
        context['more_voted'] = self.more_voted
        return context
more_voted = MoreVotedPolls.as_view()


class VoteAveragePolls(TemplateView):
    model = Poll
    context_object_name = 'vote_average'
    template_name = 'polls/vote_average.html'

    def dispatch(self, request, *args, **kwargs):
        self.vote_average = Poll.objects.vote_average()
        return super(VoteAveragePolls, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(VoteAveragePolls, self).get_context_data(**kwargs)
        context['vote_average'] = self.vote_average
        return context
vote_average = VoteAveragePolls.as_view()


class PollVote(FormView):
    form_class = VoteForm
    template_name = 'polls/vote.html'

    @method_decorator(login_required)
    @method_decorator(permission_required('polls.puede_votar', login_url='/polls/error'))
    def dispatch(self, request, *args, **kwargs):
        self.poll = get_object_or_404(Poll, pk=kwargs['poll_id'])
        return super(PollVote, self).dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(PollVote, self).get_form_kwargs()
        kwargs.update({'poll': self.poll})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(PollVote, self).get_context_data(**kwargs)
        context['poll'] = self.poll
        return context

    def form_valid(self, form):
        choice = form.cleaned_data['choice']
        choice.votes += 1
        choice.save()
        return redirect('polls:id_result', poll_id=self.poll.id)
vote = PollVote.as_view()


@login_required
@permission_required('polls.add_poll', login_url='/polls/error')
@permission_required('polls.add_choice', login_url='/polls/error')
def poll_manage(request, poll_id=None):
    ChoiceFormSet = inlineformset_factory(Poll, Choice, exclude=('votes',), formset=BaseFormSet, can_order=True, can_delete=False, extra=2)
    poll = None
    if(poll_id):
        poll = get_object_or_404(Poll, pk=poll_id)
        ChoiceFormSet = inlineformset_factory(Poll, Choice, exclude=('votes',), formset=BaseFormSet, can_order=True, can_delete=True, extra=0)

    if request.method == 'POST':
        if 'guarda_poll' in request.POST:
            form_poll = PollForm(request.POST, instance=poll)
            if form_poll.is_valid():
                poll = form_poll.save()

                formset_choice_poll = ChoiceFormSet(request.POST, instance=poll)
                if formset_choice_poll.has_changed():
                    formset_choice_poll.save()

                    list_order = []
                    for form in formset_choice_poll.ordered_forms:
                        if(form.cleaned_data['id']):
                            list_order.append(form.cleaned_data['id'].id)
                    poll.set_choice_order(list_order)

                return redirect('polls:index')
            else:
                formset_choice_poll = ChoiceFormSet(request.POST, instance=poll)
    else:
        form_poll = PollForm(instance=poll)
        formset_choice_poll = ChoiceFormSet(instance=poll)

    context = {
        'form': form_poll,
        'formset_choice_poll': formset_choice_poll,
    }
    return render(request, 'polls/poll.html', context)


class Error(TemplateView):
    template_name = 'polls/error.html'
error = Error.as_view()
